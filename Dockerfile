FROM microsoft/aspnet:1.0.0-beta8

RUN apt-get -qq update && apt-get -qqy install unzip libc6-dev && rm -rf /var/lib/apt/lists/*

ADD ./src /app
WORKDIR /app/LendFoundry.Queue.Api

ENV MONO_THREADS_PER_CPU 2000
RUN dnu restore --ignore-failed-sources -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/

EXPOSE 5000
ENTRYPOINT dnx kestrel
